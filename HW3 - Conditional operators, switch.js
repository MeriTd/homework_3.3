/* 
 Теоретичні питання
 1. Що таке логічний оператор?
 2. Які логічні оператори є в JavaScript і які їх символи?

 
 Практичні завдання.
 1. Попросіть користувача ввести свій вік за допомогою prompt. 
 Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те, 
 що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те, 
 що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

 Завдання з підвищенною складністю.
 Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.

 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення, 
 скільки днів у цьому місяці. Результат виводиться в консоль.
 Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
 (Використайте switch case)
 */




/* 
 Теоретичні питання
 1. Що таке логічний оператор?
 Це оператори, які використовуються для визначення логіки між змінними або значеннями, для перевірки true або false.


 2. Які логічні оператори є в JavaScript і які їх символи?
 Оператор “АБО” - ||
 Повертає true якщо хоча б одне значення рівне true, інакше повертає false. АБО "||" знаходить перше правдиве значення
 У більшості випадків АБО || використовується в інструкціях if, щоб перевірити, чи є будь-яка із заданих умов true.

 Оператор НЕ - !
 Повертає протилежне значення. Тобто якщо до true - поверне false, якщо до false - true.
 Також використовується подвійне заперечення !!
 Тобто, перший НЕ перетворює значення на булеве і повертає зворотне, а другий НЕ інвертує його знову. 

 Оператор І - &&
 Повертає true якщо значення рівні true, інакше повертає false. Шукає перше хибне значення

 Оператор об’єднання з null - ??
 ?? повертає перший аргумент, якщо він не null/undefined

 */






//  Практичні завдання.
// 1.


const userAge = prompt('How old are you?')


if (!userAge || isNaN(userAge)) {
    alert('Not a Number')
} else if (userAge < 0 || userAge > 150) {
    alert('Incorrect age')
} else {
    if (userAge < 12) {
        alert('You are a child')
    } else if (userAge <= 18) {
        alert('You are a teenager')
    } else {
        alert('You are an adult')
    }

}




// 2.

const month = prompt('Ведіть місяць року (українською мовою маленкими літерами)')

switch (month) {
    case 'січень': {
        console.log('31');
        break;
    }

    case 'лютий': {
        console.log('28, але в високосний рік - 29');
        break;
    }

    case 'березень': {
        console.log('31');
        break;
    }

    case 'квітень': {
        console.log('30');
        break;
    }

    case 'травень': {
        console.log('31');
        break;
    }

    case 'червень': {
        console.log('30');
        break;
    }

    case 'липень': {
        console.log('31');
        break;
    }

    case 'серпень': {
        console.log('31');
        break;
    }

    case 'вересень': {
        console.log('30');
        break;
    }

    case 'жовтень': {
        console.log('31');
        break;
    }

    case 'листопад': {
        console.log('30');
        break;
    }

    case 'грудень': {
        console.log('31');
        break;
    }

    default: {
        console.log('Не розумію')
    }
}

